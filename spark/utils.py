from matplotlib import pyplot as plt


def plot_scores(scores, k_search_range):
    plt.plot(k_search_range, scores)
    plt.xlabel('K')
    plt.ylabel('Silhouette score')
    plt.title('Silhouette Score chart')
    plt.show()
