from pyspark.sql import SparkSession

from spark.config import CONFIG




class spark_session:
    def __init__(self):
        self._spark_session = SparkSession.builder \
            .master(CONFIG['spark']['master']) \
            .appName(CONFIG['spark']['appName']) \
            .config("spark.driver.memory", CONFIG['spark']['driver.memory']) \
            .config("spark.executor.memory", CONFIG['spark']['executor.memory']) \
            .config("spark.driver.cores", CONFIG['spark']['driver.cores']) \
            .config("spark.executor.cores", CONFIG['spark']['executor.cores']) \
            .getOrCreate()

    def __enter__(self) -> SparkSession:
        return self._spark_session

    def __exit__(self, *exc):
        self._spark_session.stop()
