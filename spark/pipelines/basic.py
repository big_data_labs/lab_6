from pydantic import BaseModel

from spark.dataloader import DatasetLoader
from spark.models.knn import KMeansClustering
from spark.models.scaler import Scaler
from spark.models.vectorizer import Vectorizer
from spark.mongodb.repository import insert_data
from spark.session import spark_session
from spark.utils import plot_scores


class ResultModel(BaseModel):
    result: list[float]


class InputModel(BaseModel):
    input: list[float]


def basic_pipeline():
    with spark_session() as session:
        dataset_loader = DatasetLoader(spark_session=session)

        vectorizer = Vectorizer()
        scaler = Scaler()
        knn_clustering = KMeansClustering()

        dataset = dataset_loader.load_dataset()
        vectorized_dataset = vectorizer.vectorize(dataset)
        scaled_dataset = scaler.scale(vectorized_dataset)

        scores = knn_clustering.clusterize(scaled_dataset)
        plot_scores(scores, knn_clustering.k_search_range)

    insert_data([ResultModel(result=scores)])