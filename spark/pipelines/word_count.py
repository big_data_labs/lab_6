import os.path
from pathlib import Path

import findspark
from pyspark import SparkContext
from pyspark.sql import SparkSession

from spark.session import spark_session


def exec_word_count(filepath: Path, spark_context: SparkContext):
    text_file = spark_context.textFile(str(filepath))
    return (
        text_file.flatMap(lambda line: line.split(" "))
        .map(lambda word: (word, 1))
        .reduceByKey(lambda x, y: x + y)
        .collect()
    )


def word_count_pipeline(file_path: Path):
    if not os.path.exists(file_path):
        raise FileNotFoundError(file_path)

    findspark.init()
    with spark_session() as session:
        result = exec_word_count(file_path, session.sparkContext)
        for word, count in result:
            print(word, count, sep=": ")

