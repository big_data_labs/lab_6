from loguru import logger
from pyspark.ml.clustering import KMeans
from pyspark.ml.evaluation import ClusteringEvaluator
from pyspark.sql import DataFrame

from spark.config import CONFIG


class KMeansClustering:
    def __init__(self):
        self.features_column = CONFIG['scaler']['scaledColumnName']
        self.evaluator = ClusteringEvaluator(
            featuresCol=self.features_column,
            predictionCol=CONFIG['clusterizer']['predictionCol'],
            metricName=CONFIG['clusterizer']['metricName'],
            distanceMeasure=CONFIG['clusterizer']['distanceMeasure'],
        )

        self.k_search_range = range(2, 7)

    def clusterize(self, dataset: DataFrame):
        silhouette_scores = []
        for k in self.k_search_range:
            kmeans = KMeans(featuresCol=self.features_column, k=k)
            model = kmeans.fit(dataset)
            predictions = model.transform(dataset)
            score = self.evaluator.evaluate(predictions)

            silhouette_scores.append(score)
            logger.info(f'Silhouette Score for k = {k} is {score}')

        return silhouette_scores
