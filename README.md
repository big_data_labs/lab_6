# Модель кластеризации на PySpark

## Цель работы
Получить навыки разработки и настройки Spark приложения.

# Состав проекта

Проект включает следующие компоненты:

- **MongoDB**: Используется как основная база данных для хранения данных задач и их результатов.
- **Mongo Express**: Веб-интерфейс для работы с MongoDB, который позволяет легко просматривать и управлять данными в базе.

## Установка и запуск

1. Установите зависимости:

```bash
poetry install
```

2. Запуск сценария word count:

```bash
docker-compose up --build
```

Пример кода вставки данных в MongoDB

```python
def insert_data(results: list[BaseModel]) -> None:
    collection.insert_many([r.dict() for r in results])
    logger.info("Successfully inserted into the database")
```